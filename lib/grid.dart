import 'package:appentusmachinetest/Apiservices.dart';
import 'package:appentusmachinetest/model.dart';
import 'package:flutter/material.dart';

class GridList extends StatefulWidget {
  @override
  _GridListState createState() => _GridListState();
}

class _GridListState extends State<GridList> {
  List<Model>? list = [];
  ApiServices apiServices = ApiServices();
  @override
  void initState() {
    getList();
    super.initState();
  }

  getList() async {
    list = await apiServices.getList();
    setState(() {});
    print(list);
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisSpacing: 6, crossAxisCount: 2, mainAxisSpacing: 8),
      itemCount: list!.length,
      itemBuilder: (BuildContext context, int index) {
        print("object");
        return Stack(children: [
          Container(
            height: list![index].height!.toDouble(),
            width: list![index].width!.toDouble(),
            child:
                // Text(list![index].id.toString()),

                Image.network(
              list![index].downloadUrl!.toString(),
              fit: BoxFit.contain,
            ),
          ),
          Text(list![index].author!.toString()),
        ]);
      },
    );
  }
}
