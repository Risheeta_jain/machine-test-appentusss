class User {
  String? name;
  int? id;
  String? email;
  String? password;
  String? mobile;
  String? image;
  User(
      {this.name, this.email, this.password, this.id, this.mobile, this.image});
  MaptoUserMap() {
    return {
      'name': name,
      'email': email,
      'password': password,
      'mobile': mobile,
      'image': image,
      'id': id
    };
  }

  static fromMap(Map c) {
    return User(
        name: c['name'],
        email: c['email'],
        password: c['passowrd'],
        mobile: c['mobile'],
        image: c['image']);
  }
}
