import 'package:appentusmachinetest/Apiservices.dart';
import 'package:appentusmachinetest/SignUp.dart';
import 'package:appentusmachinetest/Utility.dart';
import 'package:appentusmachinetest/grid.dart';
import 'package:appentusmachinetest/login.dart';
import 'package:appentusmachinetest/main.dart';
import 'package:appentusmachinetest/map.dart';
import 'package:appentusmachinetest/user.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  User user;
  HomePage(this.user);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String name = '';
  ApiServices apiServices = ApiServices();
  @override
  void initState() {
    apiServices.getList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text("Hello ${widget.user.name}"),
            actions: [
              CircleAvatar(
                radius: 80,
                backgroundImage: MemoryImage(
                    Utility.dataFromBase64String(widget.user.image.toString())),
              ),
              IconButton(onPressed: (){
                 Navigator.of(context).pushReplacement(MaterialPageRoute(
                                  builder: (context) => LoginPage()));
              }, icon: Icon(Icons.logout))
            ],
            bottom: TabBar(
              tabs: [
                Tab(
                  icon: Icon(Icons.list_alt),
                  child: Text("List"),
                ),
                Tab(
                  icon: Icon(Icons.map),
                  child: Text("Map"),
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [MyApps(), GridList()],
          ),
        ));
  }
}
