import 'dart:io';

import 'package:appentusmachinetest/Utility.dart';
import 'package:appentusmachinetest/homepage.dart';
import 'package:appentusmachinetest/login.dart';
import 'package:appentusmachinetest/sqflite.dart';
import 'package:appentusmachinetest/user.dart';
import 'package:flutter/material.dart';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class SignupPage extends StatefulWidget {
  @override
  State createState() {
    // TODO: implement createState
    return SignUpState();
  }
}

class SignUpState extends State {
  final _formKey = GlobalKey();
  bool imageIs = false;
  final _scafoldKey = GlobalKey();
  final _nameEditController = TextEditingController();
  final _emailEditController = TextEditingController();
  final _mobileEditController = TextEditingController();
  final _passwordEditController = TextEditingController();

  String email_pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  String? password_pattern = r'^[a-zA-Z0-9]{6,}$';
  String? mobile_pattern =
      r'^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$';
  late Size size;
  File? image;
  String? mail, mobile, password, name;
  String string = "";
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile =
        await picker.getImage(source: ImageSource.gallery).then((value) async {
      string = Utility.base64String(await value!.readAsBytes());
      print(string);
    });

    setState(() {
      if (pickedFile != null) {
        image = File(pickedFile.path);
        setState(() {
          imageIs = false;
        });
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return new Scaffold(
      key: _scafoldKey,
      body: Stack(
        children: [
          Container(
            color: const Color(0x99FFFFFF),
          ),
          Container(
            height: 120,
            decoration: new BoxDecoration(
              border: Border.all(color: Colors.teal),
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(size.width / 2),
                  topRight: Radius.circular(size.width / 2)),
              color: Colors.teal,
            ),
          ),
          Center(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 115,
                          width: 115,
                          child: Stack(
                            fit: StackFit.expand,
                            // overflow: Overflow.visible,
                            children: [
                              CircleAvatar(
                                  backgroundImage: MemoryImage(
                                      Utility.dataFromBase64String(string))),
                              Positioned(
                                right: -16,
                                bottom: 0,
                                child: SizedBox(
                                  height: 55,
                                  width: 50,
                                  child: FlatButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50),
                                      side: BorderSide(color: Colors.white),
                                    ),
                                    color: Color(0xFFF5F6F9),
                                    onPressed: () {
                                      getImage();
                                    },
                                    // label: Container(),
                                    child: Icon(
                                      Icons.add_a_photo,
                                      size: 15,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          decoration: new BoxDecoration(
                            border: new Border.all(color: Colors.teal),
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.teal,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Registration Form",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 22),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        //--------------Name FormFiled------------------------------------------
                        TextFormField(
                          onChanged: (value) {
                            name = value;
                          },
                          controller: _nameEditController,
                          textInputAction: TextInputAction.next,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Enter Name";
                            }
                            return null;
                          },
                          style: getTextStyle(),
                          decoration: customInputDecoration("Enter Name"),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        //--------------Email FormFiled------------------------------------------
                        TextFormField(
                          onChanged: (value) {
                            mail = value;
                          },
                          controller: _emailEditController,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.emailAddress,
                          style: getTextStyle(),
                          decoration: customInputDecoration("Enter Email ID"),
                        ),
                        SizedBox(
                          height: 20,
                        ),

                        //--------------Mobile FormFiled------------------------------------------
                        TextFormField(
                          onChanged: (value) {
                            mobile = value;
                          },
                          controller: _mobileEditController,
                          textInputAction: TextInputAction.next,
                          validator: (value) {
                            if (value!.length < 10)
                              return 'Enter valid Mobile Number';
                            else
                              return null;
                          },
                          keyboardType: TextInputType.number,
                          maxLength: 10,
                          style: getTextStyle(),
                          decoration:
                              customInputDecoration("Enter mobile number"),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        //--------------Password FormFiled------------------------------------------
                        TextFormField(
                          onChanged: (value) {
                            password = value;
                          },
                          controller: _passwordEditController,
                          textInputAction: TextInputAction.done,
                          obscureText: true,
                          style: getTextStyle(),
                          decoration: customInputDecoration("Enter password"),
                        ),

                        SizedBox(
                          height: 20,
                        ),
                        RaisedButton(
                            onPressed: () {
                              User user = User(
                                name: name,
                                email: mail,
                                password: password,
                                mobile: mobile,
                                image: string,
                              );
                              UserDatabase.instance.insertUser(user);
                              print(user);
                              Navigator.of(context).pushReplacement(MaterialPageRoute(
                                  builder: (context) => HomePage(user)));
                            },
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18),
                            ),
                            color: Colors.pink,
                            child: Text(
                              "Signup",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            )),

                        TextButton(
                          child: Text("Already have account, Sign In?"),
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => LoginPage()));
                          },
                        )
                      ],
                    )),
              ),
            ),
          )
        ],
      ),
    );
    ;
  }

  TextStyle getTextStyle() {
    return TextStyle(fontSize: 18, color: Colors.pink);
  }

  InputDecoration customInputDecoration(String hint) {
    return InputDecoration(
      hintText: hint,
      hintStyle: TextStyle(color: Colors.teal),
      contentPadding: EdgeInsets.all(10),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: BorderSide(color: Colors.pink)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: Colors.pink)),
    );
  }
}
