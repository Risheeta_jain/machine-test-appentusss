import 'package:appentusmachinetest/SignUp.dart';
import 'package:appentusmachinetest/homepage.dart';
import 'package:appentusmachinetest/sqflite.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LoginPage extends StatefulWidget {
  @override
  State createState() {
    // TODO: implement createState
    return LoginState();
  }
}

class LoginState extends State {
  final _formKey = GlobalKey();
  final _scaffoldKey = GlobalKey();
  final _mobileController = TextEditingController();
  final _passwordController = TextEditingController();
  final FocusNode _mobileFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  late Size size;
  String? mail, password;
  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return new Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: [
          Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      onChanged: (valu) {
                        mail = valu;
                      },
                      controller: _mobileController,
                      textInputAction: TextInputAction.next,
                      focusNode: _mobileFocus,
                      onFieldSubmitted: (term) {
                        FocusScope.of(context).requestFocus(_passwordFocus);
                      },
                      validator: (String? value) {},
                      style: getTextStyle(),
                      decoration: customInputDecoration("Enter Mobile ID"),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      onChanged: (value) {
                        password = value;
                      },
                      textInputAction: TextInputAction.done,
                      controller: _passwordController,
                      keyboardType: TextInputType.text,
                      obscureText: true,
                      focusNode: _passwordFocus,
                      style: getTextStyle(),
                      decoration: customInputDecoration("Enter password"),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    RaisedButton(
                      onPressed: () {
                        if (mail == "" || password == "") {
                          Fluttertoast.showToast(
                              msg: "Please enter something ",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.black,
                              textColor: Colors.white,
                              fontSize: 16.0);
                        }
                        UserDatabase.instance.getUsers();
                      UserDatabase.instance
                            .checkUserLogin(email: mail, password: password)
                            .then((result) {
                          if (result == []) {
                            Fluttertoast.showToast(
                                msg: "Please enter valid details",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.black,
                                textColor: Colors.white,
                                fontSize: 16.0);
                          } else {
                            Navigator.of(context).pushReplacement(MaterialPageRoute(
                                builder: (context) => HomePage(result)));
                          }
                        });
                      },
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18),
                      ),
                      color: Colors.pink,
                      child: Text(
                        "Login",
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                    FlatButton(
                      child: Text("Don't have account, Signup?"),
                      onPressed: () {
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (context) => SignupPage()));
                      },
                    )
                  ],
                )),
          )
        ],
      ),
    );
  }

  TextStyle getTextStyle() {
    return TextStyle(fontSize: 18, color: Colors.pink);
  }

  InputDecoration customInputDecoration(String hint) {
    return InputDecoration(
      hintText: hint,
      hintStyle: TextStyle(color: Colors.teal),
      contentPadding: EdgeInsets.all(10),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: BorderSide(color: Colors.pink)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: Colors.pink)),
    );
  }
}
