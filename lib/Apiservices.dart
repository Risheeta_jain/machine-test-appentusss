import 'dart:convert';
import 'dart:io';

import 'package:appentusmachinetest/model.dart';
import 'package:http/http.dart' as http;

class ApiServices {
 Future<List<Model>?> getList() async {
    String url = "https://picsum.photos/v2/list";
    try {
      final response = await http.get(Uri.parse(url));
      if (response.statusCode != 200)
        throw HttpException('${response.statusCode}');
      

      var jsonMap = jsonDecode(response.body);
      List<dynamic> data = jsonMap;
     
      List<Model> rs = List<Model>.from(data.map((e) => Model.fromJson(e)));
      print(rs.length);
      return rs;
    } on SocketException {
      print('No Internet connection ');
    } on HttpException {
      print("Couldn't find the post ");
    } on FormatException {
      print("Bad response format ");
    }
  }
}
