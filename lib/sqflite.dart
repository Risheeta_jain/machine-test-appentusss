import 'dart:io';
import 'package:appentusmachinetest/user.dart';
import 'package:flutter/foundation.dart' as isKwb;
import 'package:path/path.dart';

import 'package:sqflite/sqflite.dart';

import 'package:path_provider/path_provider.dart';

class UserDatabase {
  static late String path;
  static final _databaseName = "mydb.db";
  static final _databaseVersion = 1;

  static final _table_user = 'users';
  static final _table_logins = 'logins';

  UserDatabase._privateConstructor();
  static final UserDatabase instance = UserDatabase._privateConstructor();

  // only have a single app-wide reference to the database
  Database? _database;

  Future get database async {
    if (_database != null) return _database;
    // lazily instantiate the db the first time it is accessed
    if (_database == null) {
      _database = await _initDatabase();
      return _database;
    }
  }

  // this opens the database (and creates it if it doesn't exist)
  _initDatabase() async {
    if (isKwb.kIsWeb) {
      print("iswb");
    } else {
      Directory documentsDirectory = await getApplicationDocumentsDirectory();
      String path = join(documentsDirectory.path, _databaseName);
      print("bas on");
      return await openDatabase(path,
          version: _databaseVersion, onCreate: _onCreate);
    }
  }

  // SQL code to create the database table
  Future _onCreate(Database db, int version) async {
    print("tabl");
    await db.execute(
      "CREATE TABLE users(id INTEGER PRIMARY KEY autoincrement, name TEXT, email VARCHAR,  password TEXT, mobile TEXT, image TEXT)",
    );
    await db.execute(
      "CREATE TABLE logins(name TEXT, email VARCHAR ,password TEXT , mobile TEXT,image TEXT)",
    );
  }

  static Future getFileData() async {
    return getDatabasesPath().then((s) {
      return path = s;
    });
  }

  Future insertUser(User user) async {
    Database db = await instance.database;
    print("insrtUsr");

    return await db.insert("users", user.MaptoUserMap(),
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }

  Future checkUserLogin({String? email, String? password}) async {
    Database db = await instance.database;
    List<User> list = [];
    print("ass+${email! + password.toString()}");
    var res = await db.rawQuery(
        "select * from users where email = '$email' and password = '$password'");
    print(res);
    if (res.length > 0) {
      for (var i in res) {
        User user = User.fromMap(i);

        list.add(user);
      }
      return list[0];
    }
    return list[0];
  }

  Future getUser() async {
    Database db = await instance.database;
    var logins = await db.rawQuery("select * from logins");
    if (logins == null) return 0;
    return logins.length;
  }

  Future getUserData() async {
    Database db = await instance.database;
    var res = await db.rawQuery("select * from users");

    print("result user data gf" + res.toString());
    List list = res.toList().map((c) => User.fromMap(c)).toList();
    return list;
  }

  Future<List<User>> getUsers() async {
    Database db = await instance.database;
    List<User> list = [];
    var res = await db.rawQuery("select * from users");
    print("result user data $res");
    print("result user data " + res.toString());
    for (var i in res) {
      User user = User.fromMap(i);

      list.add(user);
    }
    return list;
  }

  Future deleteUser(String email) async {
    Database db = await instance.database;
    print("om");
    int logins =
        await db.delete(_table_user, where: "email = ?", whereArgs: [email]);
    print(logins);
    return logins;
  }

  Future updateUser({String? email, String? user}) async {
    Database db = await instance.database;
    print("om");
    var login = await db.rawQuery(
        """ UPDATE User SET email = ${email.toString()} WHERE name = $user; """);
    // int logins = await db.update(_table_user, user.MaptoUserMap(), whereArgs: [
    //  {"email": email}
    // ]);
    print(login);
    //return logins;
  }
}
